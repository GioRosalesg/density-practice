## Backend
We want to create a grades’ system where only some registered admin users can access it and handle the CRUD of students, courses, and regarding a specific student, the CRUD of its grades as well.
 
A student should have at least 2 fields for identifying it, and a course at least one. 
 
A student can take more than one course at a time
 
We only want to authorize full read/write access to the system to some registered users (admins, not teachers).
 
We want to authorize the students to have read access to their own courses and grades
 
### Notes:
- A course is year-long and is evaluated per year quarters, that is, the student will be examined on the course four times, (Quarter1, Quarter2, Quarter3 and Quarter4)
- A student can’t have more than one grade per course per quarter
- A grade is a number that could go from 0 to 10, 0-5 is considered “failed”, 6-10 is “passed”.
- We’d like to support student email field and send an email to the student when there is an update on any of its grades or a new grade
- You are free to assume everything that is not explained here.
 
 
## Frontend
### Admin
- List of all students
- List of a student’ grades, per quarter. Indicate whether it’s passed or failed.
- List of all courses
- List of a course’ students
- Forms to create students, courses and grades
- Login/sign in form to access system
- Ability to sign out
### Student
- List of all their courses per year
- List of each of their course’ grades
- Login form to access system
- Ability to sign out