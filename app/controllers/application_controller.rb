class ApplicationController < ActionController::Base
  def verify_authentication
    redirect_to(root_path, alert: "Unauthorized resource, please login to continue...") unless current_student || current_admin
  end

  def verify_admin
    redirect_to(root_path, alert: "Unauthorized resource, please login as admin to continue...") unless current_admin
  end
end
