class EnrollmentsController < ApplicationController
  before_action :set_enrollment, only: %i[show edit update destroy]
  before_action :verify_authentication, only: %i[index show]
  before_action :verify_admin, except: %i[index show]

  def index
    if current_admin
      @enrollments = Enrollment.includes(:student, :course).all
    else
      @enrollments = Enrollment.includes(:student, :course).where(student_id: current_student.id)
    end
    @enrollments = @enrollments.order(year: :desc)
  end

  def new
    @enrollment = Enrollment.new
  end

  def create
    @enrollment = Enrollment.new(enrollment_params)

    respond_to do |format|
      if @enrollment.save
        EnrollmentMailer.with(enrollment: @enrollment).new_enrollment.deliver_later
        format.html { redirect_to enrollments_url, notice: "Enrollment was successfully created." }
        format.json { render :show, status: :created, location: @enrollment }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @enrollment.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
  end

  def edit
  end

  def update
    respond_to do |format|
      if @enrollment.update(enrollment_params)
        format.html { redirect_to enrollments_url, notice: "Enrollment was successfully updated." }
        format.json { render :show, status: :created, location: @enrollment }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @enrollment.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @enrollment.destroy!

    respond_to do |format|
      format.html { redirect_to enrollments_url, notice: "Enrollment was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
  def set_enrollment
    @enrollment = Enrollment.includes(:student, :course).find(params[:id])
  end

  def enrollment_params
    params.require(:enrollment).permit(:student_id, :course_id, :year)
  end
end
