class QuartersController < ApplicationController
  before_action :verify_admin
  before_action :set_enrollment, only: %i[new create edit update]
  before_action :set_quarter, only: %i[edit update destroy]

  def index
  end

  def new
    @quarter = Quarter.new
  end

  def create
    @quarter = @enrollment.quarters.new(quarter_params)

    respond_to do |format|
      if @quarter.save
        QuarterMailer.with(quarter: @quarter).new_grade.deliver_later
        format.html { redirect_to enrollment_url(@enrollment), notice: "quarter was successfully created." }
        format.json { render :show, status: :created }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @quarter.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
  end
  
  def update
    respond_to do |format|
      if @quarter.update(quarter_params)
        QuarterMailer.with(quarter: @quarter).update_grade.deliver_later
        format.html { redirect_to enrollment_url(@enrollment), notice: "quarter was successfully updated." }
        format.json { render :show, status: :created }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @quarter.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
  end

  private
  def set_enrollment
    @enrollment = Enrollment.find(params[:enrollment_id])
  end
  def set_quarter
    @quarter = Quarter.find_by(id: params[:id], enrollment_id: params[:enrollment_id])
  end
  def quarter_params
    params.require(:quarter).permit(:grade, :quarter_number)
  end
end