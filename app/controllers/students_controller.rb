class StudentsController < ApplicationController
  before_action :set_student, only: %i[ show edit update destroy ]
  before_action :verify_admin, except: %i[ show edit update ]
  before_action :verify_authentication, only: %i[ show edit update ]
  before_action :verify_same_student, only: %i[ show edit update ]
  before_action :allow_without_password, only: %i[ update ]

  # GET /students or /students.json
  def index
    @students = Student.active
  end

  # GET /students/1 or /students/1.json
  def show
  end

  # GET /students/new
  def new
    @student = Student.new
  end

  # GET /students/1/edit
  def edit
  end

  # POST /students or /students.json
  def create
    temporal_password = Devise.friendly_token.first(8)
    @student = Student.new(student_params)
    @student.password = temporal_password

    respond_to do |format|
      if @student.save
        StudentMailer.with(student: @student, temporal_password: temporal_password).welcome_email.deliver_later
        format.html { redirect_to student_url(@student), notice: "Student was successfully created." }
        format.json { render :show, status: :created, location: @student }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /students/1 or /students/1.json
  def update
    respond_to do |format|
      if @student.update(student_update_params)
        bypass_sign_in(@student)
        format.html { redirect_to student_url(@student), notice: "Student was successfully updated." }
        format.json { render :show, status: :ok, location: @student }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /students/1 or /students/1.json
  def destroy
    @student.update(active: false)

    respond_to do |format|
      format.html { redirect_to students_url, notice: "Student was successfully deactivated." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_student
      @student = Student.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def student_params
      params.require(:student).permit(:fullname, :email)
    end

    def student_update_params
      params.require(:student).permit(:fullname, :email, :password, :password_confirmation)
    end

    def verify_same_student
      if current_student
        redirect_to(enrollments_path, alert: "You cannot access to different student information.") unless current_student.id == @student.id
      end
    end

    def allow_without_password
      if params[:student][:password].blank? && params[:student][:password_confirmation].blank?
          params[:student].delete(:password)
          params[:student].delete(:password_confirmation)
      end
    end
end
