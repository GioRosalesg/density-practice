class EnrollmentMailer < ApplicationMailer
  def new_enrollment
    @enrollment = params[:enrollment]
    @student = @enrollment.student
    @course = @enrollment.course
    @url = enrollment_url(@enrollment.id)
    mail(to: @student.email, subject: "Enrollment to #{@course.name}")
  end
end