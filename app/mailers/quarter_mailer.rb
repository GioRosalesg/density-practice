class QuarterMailer < ApplicationMailer
  def new_grade
    @quarter = params[:quarter]
    @enrollment = @quarter.enrollment
    @student = @enrollment.student
    @course = @enrollment.course
    @url = enrollment_url(id: @enrollment.id)
    mail(to: @student.email, subject: "You got a new grade for #{@course.name}")
  end

  def update_grade
    @quarter = params[:quarter]
    @enrollment = @quarter.enrollment
    @student = @enrollment.student
    @course = @enrollment.course
    @url = enrollment_url(id: @enrollment.id)
    mail(to: @student.email, subject: "A grade for #{@course.name} was updated")
  end
end