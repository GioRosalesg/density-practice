class StudentMailer < ApplicationMailer
  def welcome_email
    @student = params[:student]
    @temporal_password = params[:temporal_password]
    @url = new_student_session_url
    mail(to: @student.email, subject: "Welcome to this amazing [insert schoool name]")
  end
end
