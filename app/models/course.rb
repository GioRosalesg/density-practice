class Course < ApplicationRecord
  # Relations
  has_many :enrollments
  has_many :students, through: :enrollments

  # Validations
  validates :name, presence: true

  # Scopes
  scope :active, -> { where(active: true) }
  scope :inactive, -> { where(active: false) }
end
