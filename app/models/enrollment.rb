class Enrollment < ApplicationRecord
  # Relations
  belongs_to :course
  belongs_to :student
  has_many :quarters, dependent: :delete_all

  # Validations
  validates :course_id, presence: true
  validates :student_id, presence: true
  validates :year, presence: true
  validates :year, numericality: {
    only_integer: true, 
    greater_than_or_equal_to: 1900,
    less_than_or_equal_to: Date.today.year
  }
  validates_uniqueness_of :student_id, {scope: [:course_id, :year], message: "already enrolled on selected course and year."}
end
