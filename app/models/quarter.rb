class Quarter < ApplicationRecord
  # Relations
  belongs_to :enrollment

  # Validations
  validates :quarter_number, presence: true
  validates :quarter_number, numericality: {
    only_integer: true,
    greater_than_or_equal_to: 1,
    less_than_or_equal_to: 4
  }
  validates :grade, presence: true
  validates :grade, numericality: {
    only_integer: true,
    greater_than_or_equal_to: 0,
    less_than_or_equal_to: 10
  }
  validates_uniqueness_of :quarter_number, {scope: :enrollment_id, message: "already has a grade"}

  def passed?
    grade > 5
  end
end
