class Student < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :validatable

  # Relations
  has_many :enrollments
  has_many :courses, through: :enrollments

  # Validations
  validates :fullname, presence: true
  validates :email, presence: true
  validates :email, uniqueness: true

  # Scopes
  scope :active, -> { where(active: true) }
  scope :inactive, -> { where(active: false) }
end
