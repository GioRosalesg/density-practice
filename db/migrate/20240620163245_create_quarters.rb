class CreateQuarters < ActiveRecord::Migration[7.1]
  def change
    create_table :quarters do |t|
      t.integer :quarter_number
      t.integer :grade

      t.references :enrollment, null: true, foreign_key: true

      t.timestamps
    end
  end
end
