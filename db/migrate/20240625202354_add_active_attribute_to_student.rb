class AddActiveAttributeToStudent < ActiveRecord::Migration[7.1]
  def change
    add_column :students, :active, :boolean, default: true
  end
end
