class AddActiveAttributeToCourse < ActiveRecord::Migration[7.1]
  def change
    add_column :courses, :active, :boolean, default: true
  end
end
