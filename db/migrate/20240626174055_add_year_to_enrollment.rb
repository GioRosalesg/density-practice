class AddYearToEnrollment < ActiveRecord::Migration[7.1]
  def change
    add_column :enrollments, :year, :integer, null: false, default: 0
  end
end
