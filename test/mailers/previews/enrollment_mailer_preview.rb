class EnrollmentMailerPreview < ActionMailer::Preview
  def new_enrollment
    EnrollmentMailer.with(enrollment: Student.first.enrollments[0]).new_enrollment
  end
end