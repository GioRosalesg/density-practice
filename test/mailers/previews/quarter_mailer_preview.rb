class QuarterMailerPreview < ActionMailer::Preview
  def new_quarter
    QuarterMailer.with(quarter: Quarter.first).new_grade
  end

  def update_grade
    QuarterMailer.with(quarter: Quarter.first).update_grade
  end
end